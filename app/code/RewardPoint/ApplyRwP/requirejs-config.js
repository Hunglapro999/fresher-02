var config = {
    paths: {
        jquerymin: 'js/jquery.min'
    },
    shim: {
        jquerymin: {
            deps: ['jquery']
        }
    }
};
   