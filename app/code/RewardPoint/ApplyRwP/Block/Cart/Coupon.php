<?php

namespace Team4\RewardPoint\Model\Total\Quote;

use Magento\Quote\Api\Data\ShippingAssignmentInterface;
use Magento\Quote\Model\Quote;
use Magento\Quote\Model\Quote\Address\Total;
use Magento\Quote\Model\Quote\Address\Total\AbstractTotal;

/**
 * Class PointUse
 * @package Team4\RewardPoint\Model\Total\Quote
 *
 */
class PointUse extends AbstractTotal
{
    /**
     * @param Quote $quote
     * @param ShippingAssignmentInterface $shippingAssignment
     * @param Total $total
     * @return $this|PointUse
     */
    public function collect(
        Quote $quote,
        ShippingAssignmentInterface $shippingAssignment,
        Total $total
    ) {
        parent::collect($quote, $shippingAssignment, $total);

        $subTotal = $quote->getSubtotalWithDiscount();
        $itemCount = $quote->getData('items_count');

        if ($itemCount == 0) {
            $point = $quote->setPointDiscount(0);
            $discount = $point;
            $quote->setPointUse(0);
        } else {
            $items = $shippingAssignment->getItems(); // Fix applied twice discount
            if (!count($items)) {
                return $this;
            }
            // Logic
            $point = $quote->getPointDiscount();

            if ($subTotal < $point) {
                $discount = $subTotal;
                //Transform to the real point
                $rate = $quote->getPointDiscount() / $quote->getPointUse();
                $quote->setData('point_use', floor($discount / $rate));
                $quote->setData('point_discount', $quote->getData('point_use') * $rate);

                $total->addTotalAmount($this->getCode(), -$quote->getData('point_use') * $rate);
                $total->addBaseTotalAmount($this->getCode(), -$quote->getData('point_use') * $rate);
            } else {
                $discount = $point;
                $total->addTotalAmount($this->getCode(), -$discount);
                $total->addBaseTotalAmount($this->getCode(), -$discount);
                $quote->setData('point_discount', $discount);
            }
        }

        return $this;
    }

    /**
     * @param Quote $quote
     * @param Total $total
     * @return array
     */
    public function fetch(
        Quote $quote,
        Total $total
    ) {
        $discount = $quote->getPointDiscount();
        return [
            'code' => 'point_discount',
            'title' => 'PointDiscount',
            'value' => -$discount //You can change the reduced amount, or replace it with your own variable
        ];
    }

    /**
     * @param Address\Total $total
     */
    protected function clearValues(Address\Total $total)
    {
        $total->setTotalAmount('subtotal', 0);
        $total->setBaseTotalAmount('subtotal', 0);
        $total->setTotalAmount('tax', 0);
        $total->setBaseTotalAmount('tax', 0);
        $total->setTotalAmount('discount_tax_compensation', 0);
        $total->setBaseTotalAmount('discount_tax_compensation', 0);
        $total->setTotalAmount('shipping_discount_tax_compensation', 0);
        $total->setBaseTotalAmount('shipping_discount_tax_compensation', 0);
        $total->setSubtotalInclTax(0);
        $total->setBaseSubtotalInclTax(0);
    }
}
