<?php
namespace RewardPoint\ApplyRwP\Block;

class RewardpointGet extends \Magento\Framework\View\Element\Template
{
	protected $_giftwrapFactory;
	protected $_storeManager;
	protected $_urlInterface;    
	protected $storeConfig;
	protected $currencyCode;

	public function __construct(
		\Magento\Framework\View\Element\Template\Context $context,
		\Team3Vendor\GiftwrapSlider\Model\GiftwrapFactory $giftwrapFactory,
		\Magento\Store\Model\StoreManagerInterface $storeManager, 
		\Magento\Framework\UrlInterface $urlInterface,
		\Magento\Store\Model\StoreManagerInterface $storeConfig,
		\Magento\Directory\Model\CurrencyFactory $currencyFactory,
		array $data = []
	){
		$this->_giftwrapFactory = $giftwrapFactory;
		$this->_storeManager = $storeManager; 
		$this->_urlInterface = $urlInterface;
		$this->storeConfig = $storeConfig;
        $this->currencyCode = $currencyFactory->create();
		return parent::__construct($context, $data);
	}

	public function getStoreId()
    {
        return $this->_storeManager->getStore()->getId();
    }

	public function getCollection()
	{
		return $this->_giftwrapFactory->create()
			->getCollection()
			->addFieldToFilter('store_ids', array('in' => array(0, $this->getStoreId())))
			->addFieldToFilter('status', 1);
	}
    public function getCurrentRewardPoint() {
    	$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
        $cart = $objectManager->get('\Magento\Checkout\Model\Cart');  
        return 'Used ' . $cart->getQuote()->getData('rewardpointDB'). ' reward points' ;
    }
}