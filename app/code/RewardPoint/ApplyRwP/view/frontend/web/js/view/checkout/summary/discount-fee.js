define(
    [
        'jquery',
        'Magento_Checkout/js/view/summary/abstract-total',
        'Magento_Checkout/js/model/quote',
        'Magento_Checkout/js/model/totals',
        'Magento_Catalog/js/price-utils'
    ],
    function ($, Component, quote, totals, priceUtils) {
        "use strict";
        var point = window.checkoutConfig.rewardpoint;
        var quoteData = window.checkoutConfig.quoteData;
        // var test = quoteData['rewardpoint'];
        return Component.extend({
            defaults: {
                template: 'RewardPoint_ApplyRwP/checkout/summary/discount-fee'
            },
            totals: quote.getTotals(),
            isDisplayedDiscountTotal: function () {
                // var test = totals.getSegment('discountCustom').value;
                var test = quoteData['rewardpoint'];
                if(Number(test) !== 0)
                {
                    return true;
                }
                else {
                    return false;
                }
                //return true;
            },
            getDiscountTotal: function () {
                var price = totals.getSegment('discountCustom').value;
                return this.getFormattedPrice(price);
            },
            getCurrentRewardpoint: function () {
                return point;
            },
        });
    }
);
