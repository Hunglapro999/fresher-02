<?php

namespace RewardPoint\ApplyRwP\Model;

use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Framework\View\LayoutInterface;

class ConfigProvider implements ConfigProviderInterface
{
    /** @var LayoutInterface  */
    protected $_layout;
    protected $RewardpointGet;

    public function __construct(
        LayoutInterface $layout,
        \RewardPoint\ApplyRwP\Block\RewardpointGet $RewardpointGet
    )
    {
        $this->_layout = $layout;
        $this->RewardpointGet = $RewardpointGet;
    }

    public function getConfig()
    {

        return [
            'rewardpoint' => $this->RewardpointGet->getCurrentRewardPoint()
        ];
    }
}