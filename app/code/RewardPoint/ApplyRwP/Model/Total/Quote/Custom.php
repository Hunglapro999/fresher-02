<?php
namespace RewardPoint\ApplyRwP\Model\Total\Quote;

use Magento\Quote\Api\Data\ShippingAssignmentInterface;
use Magento\Quote\Model\Quote;
use Magento\Quote\Model\Quote\Address\Total;
use Magento\Quote\Model\Quote\Address\Total\AbstractTotal;

class Custom extends AbstractTotal
{
    public function collect(
        Quote $quote,
        ShippingAssignmentInterface $shippingAssignment,
        Total $total
    ) {
       
        parent::collect($quote, $shippingAssignment, $total);
        $subTotal = $quote->getSubtotal();
        $subTotalWithDiscount = $quote->getData('subtotal_with_discount');
        $itemCount = $quote->getData('items_count');
    
        if($itemCount == 0) {
            $point = $quote->setRewardpoint(0);
            $discountCustom = $point;
            $quote->setData('rewardpointDB', 0);
            //$quote->setDiscount(0);
        }
        else {
            $items = $shippingAssignment->getItems();  // Fix applied twice discount
            if (!count($items)) {
                return $this;
            }
            // Logic 
            $point = $quote->getRewardpoint();
            if($subTotalWithDiscount < $point) {  
                // case: subtotal < discount reward : set discount = subtotal
                // subtotal = point x Point Spending Rate => subtotal/PSR = point
                // set rewardpoint for this cart = point
                $discountCustom = $subTotalWithDiscount;
                $spendRate = $quote->getEarnpoint();
                $total->addTotalAmount($this->getCode(), -$discountCustom);
                $total->addBaseTotalAmount($this->getCode(), -$discountCustom);
                // $quote->setDiscount($discount);

                //Transform to the real point
                $realPointApplied = $subTotalWithDiscount/$spendRate;
                $quote->setData('rewardpoint',$discountCustom);
                $quote->setData('rewardpointDB', $realPointApplied);
            }
            else {
                $spendRate = $quote->getEarnpoint();
                $discountCustom = $point;
                // $realPointApplied = $point/$spendRate;
                $total->addTotalAmount($this->getCode(), -$discountCustom);
                $total->addBaseTotalAmount($this->getCode(), -$discountCustom);
                // $quote->setDiscount($discount);
                //$quote->setData('rewardpointDB', $discountCustom);
 
            }

        }

        return $this;
    }

    public function fetch(
        Quote $quote,
        Total $total
    ) {
        // $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        // $cart = $objectManager->get('\Magento\Checkout\Model\Cart'); 
        // Get sub total of cart 
        $subTotal = $quote->getSubtotal();
        //$point = $cart->getQuote()->getData('rewardpoint');
        $itemCount = $quote->getData('items_count');

        if($itemCount == 0) {
            $point = $quote->setRewardpoint(0);
            $discountCustom = $point;
            $quote->setRewardpointDB(0);
            $quote->setDiscount(0);
        }
        //$point = $quote->getPoint($point);
        else {
            $point = $quote->getRewardpoint();
            if($subTotal < $point) {  
                // case: subtotal < discount reward : set discount = subtotal
                // subtotal = point x Point Spending Rate => subtotal/PSR = point
                // set rewardpoint for this cart = point
                $discountCustom = $subTotal;
                $spendRate = $quote->getEarnpoint();
                $total->addTotalAmount($this->getCode(), -$discountCustom);
                $total->addBaseTotalAmount($this->getCode(), -$discountCustom);
                // $quote->setDiscount($discount);

                //Transform to the real point
                $realPointApplied = $subTotal / $spendRate;
                $quote->setData('rewardpoint',$discountCustom);
                $quote->setData('rewardpointDB',$realPointApplied);
            }
            else {
                $discountCustom = $point;
            }
        }
        return [
            'code'  => 'discountCustom',
            'title' => 'Reward point',
            'value' => -$discountCustom  //You can change the reduced amount, or replace it with your own variable
        ];
    }
}