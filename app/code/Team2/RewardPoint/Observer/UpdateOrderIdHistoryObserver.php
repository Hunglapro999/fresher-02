<?php
namespace Team2\RewardPoint\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class UpdateOrderIdHistoryObserver implements ObserverInterface
{
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $order = $observer->getEvent()->getOrder();
        $orderId = $order->getId();
        
        $quote = $observer->getEvent()->getQuote();
        //save data
        $point = $quote->getData('rewardpoint');//reward point discount
        $customer_id = $quote->getData('customer_id');
        $rewardpointDB = $quote->getData('rewardpointDB'); //rewardpoint apply
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
                $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
                $connection = $resource->getConnection();
        if($rewardpointDB>0)
        {
            //get increment_id in sales_order 
             $sql0 = "Select * from sales_order where entity_id= ".$orderId ;
                $result = $connection->fetchAll($sql0);
                foreach ($result as $sale) {
                    $increment_id = $sale['increment_id'];
                }
            //end
            $rewardpointDB1 = $rewardpointDB*(-1);
            $tableName = $resource->getTableName('reward_point_history');
             $sql = "INSERT INTO ".$tableName." (customer_id,action,point,type,order_id,author,increment_orderid) values ($customer_id,'Order Paid',$rewardpointDB1,'shopping cart',$orderId,'Admin','#$increment_id')";
            $connection->query($sql);

            $tableName2 = "reward_point" ;
            $sql2 = "Select * FROM " . $tableName2." Where customer_id=".$customer_id;
            $result = $connection->fetchAll($sql2);
            $count = count($result);
            if($count>0)
            {
                //code update
                foreach ($result as $row) {
                $pointOld = $row["point"];
                }
                $newPoint = $pointOld - $rewardpointDB;
                $sql3 = "Update " . $tableName2 . " Set point = ".$newPoint." where customer_id = ".$customer_id;
                $connection->query($sql3);
                $sql4 = "Update sales_order set flash_rewardpoint=1, rewardpoint=".$rewardpointDB.", rewardpointDiscount= ".$point*(-1)." where entity_id=".$orderId;
                $connection->query($sql4);
            }
        }
        // return $this;
    }
}

