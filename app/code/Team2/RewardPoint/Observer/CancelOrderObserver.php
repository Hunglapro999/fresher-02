<?php
namespace Team2\RewardPoint\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;

class CancelOrderObserver implements ObserverInterface
{
    /**
     * Set payment fee to order
     *
     * @param EventObserver $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $_order = $observer->getEvent()->getOrder();
        $order_id = $_order->getId();
        $customer_id = $_order->getData("customer_id");
        if($customer_id != "")
        {
            // $reward_point = $_order->getData("base_subtotal")*$this->getPointEarnSpend();
            // $order_id = $_order->getId();

            $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
            $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
            $connection = $resource->getConnection();
            //get record History Order Paid
            $tableName = $resource->getTableName('reward_point_history');
            $sql5 = "select * from ".$tableName." WHERE order_id=".$order_id." and action like 'Order Paid%'";
            $result5 = $connection->fetchAll($sql5);
            if (count($result5)>0) {
                foreach ($result5 as $row5) {
                    $pointCancel = $row5['point']*(-1);
                }
                  //get increment_id sales_order
                $sql0 = "Select * from sales_order where entity_id= ".$order_id ;
                $result = $connection->fetchAll($sql0);
                foreach ($result as $sale) {
                    $increment_id = $sale['increment_id'];
                }
                    //end 
                $sql = "INSERT INTO ".$tableName." (customer_id,action,point,type,order_id,author,increment_orderid) values ($customer_id,'Order Cancel',$pointCancel,'shopping cart',$order_id,'Admin','#$increment_id')";
                $connection->query($sql);

                $tableName2 = "reward_point" ;
                $sql2 = "Select * FROM " . $tableName2." Where customer_id=".$customer_id;
                $result = $connection->fetchAll($sql2);
                if(count($result)>0)
                {
                    //code update
                    foreach ($result as $row) {
                    $pointOld = $row["point"];
                    }
                    $newPoint = $pointOld + $pointCancel;
                    $sql3 = "Update " . $tableName2 . " Set point = ".$newPoint." where customer_id = ".$customer_id;
                    $connection->query($sql3);
                }
            } 
            //end
          

        }
    
    }
}

