<?php

namespace Team2\RewardPoint\Block\Adminhtml\Sales;

class Totals extends \Magento\Framework\View\Element\Template
{

   
   

    /**
     * @var \Magento\Directory\Model\Currency
     */
    protected $_currency;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Directory\Model\Currency $currency,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_currency = $currency;
    }

    /**
     * Retrieve current order model instance
     *
     * @return \Magento\Sales\Model\Order
     */
    public function getOrder()
    {
        return $this->getParentBlock()->getOrder();
    }

    /**
     * @return mixed
     */
    public function getSource()
    {
        return $this->getParentBlock()->getSource();
    }

    /**
     * @return string
     */
    public function getCurrencySymbol()
    {
        return $this->_currency->getCurrencySymbol();
    }

    /**
     *
     *
     * @return $this
     */
    public function initTotals()
    {
        $this->getParentBlock();
        $this->getOrder();
        $this->getSource();
        $rw =  $this->getOrder()->getData('rewardpoint');
        if(!$this->getSource()->getRewardpoint()) {
            return $this;
        }

        // $label = 'Giftwrap ' . $this->getSource()->getGiftwrapName();
        if($this->getSource()->getData('rewardpointDiscount')!=0){
            $total = new \Magento\Framework\DataObject(
                [
                    'code' => 'rewardpointDiscount',
                    'value' => $this->getSource()->getData('rewardpointDiscount'),
                    'label' => 'Reward point(Used '.$rw.' RW point)',
                ]
            );
            $this->getParentBlock()->addTotalBefore($total, 'grand_total');
        }
        
        return $this;
    }
}
