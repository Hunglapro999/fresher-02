<?php
namespace Team1\GiftwrapSlider\Plugin;

use Magento\Framework\Pricing\PriceCurrencyInterface;

class FormatPriceDecimal
{
    protected $_storeManager;

    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ){
        $this->_storeManager = $storeManager; 
    }

    public function beforeConvertAndFormat(
        \Magento\Directory\Model\PriceCurrency $subject, 
        $amount,
        $includeContainer = true,
        $precision = PriceCurrencyInterface::DEFAULT_PRECISION,
        $scope = null,
        $currency = null)
    {
        $currentCurrencyCode = $this->getCurrentCurrencyCode();
        if ($currentCurrencyCode == "VND" && $currency == null) {
            $precision = 0;
        }

        return [$amount, $includeContainer, $precision, $scope, $currency];
    }

    public function beforeFormat(
        \Magento\Directory\Model\PriceCurrency $subject, 
        $amount,
        $includeContainer = true,
        $precision = PriceCurrencyInterface::DEFAULT_PRECISION,
        $scope = null,
        $currency = null)
    {
        $currentCurrencyCode = $this->getCurrentCurrencyCode();
        if ($currentCurrencyCode == "VND" && $currency == null) {
            $precision = 0;
        }

        return [$amount, $includeContainer, $precision, $scope, $currency];
    }

    // public function beforeConvertAndRound(
    //     \Magento\Directory\Model\PriceCurrency $subject, 
    //     $amount,
    //     $scope = null,
    //     $currency = null,
    //     $precision = PriceCurrencyInterface::DEFAULT_PRECISION)
    // {
    //     $currentCurrencyCode = $this->getCurrentCurrencyCode();
    //     if ($currentCurrencyCode == "VND") {
    //         $precision = 0;
    //     }

    //     return [$amount, $scope, $currency, $precision];
    // }

    // public function beforeRoundPrice(
    //     \Magento\Directory\Model\PriceCurrency $subject, 
    //     $price,
    //     $precision = PriceCurrencyInterface::DEFAULT_PRECISION)
    // {
    //     $currentCurrencyCode = $this->getCurrentCurrencyCode();
    //     if ($currentCurrencyCode == "VND") {
    //         $precision = 0;
    //     }

    //     return [$price, $precision];
    // }

    public function getCurrentCurrencyCode()
    {
        return $this->_storeManager->getStore()->getCurrentCurrencyCode();
    }  
}